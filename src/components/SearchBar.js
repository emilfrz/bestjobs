import React, {useEffect, useState} from 'react';
import '../scss/layout/Header.scss';
import '../scss/components/SearchBar.scss';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch,faLocationArrow} from '@fortawesome/free-solid-svg-icons'
import Form from 'react-bootstrap/Form'
import JobsListing from "./JobsListing";

const SearchBar = () => {
    let jobsDefault = [
        {
            "id": 0,
            "name": "Full-Stack Web Developer",
            "firma": "Bestjobs",
            "rating": 22,
            "location": "Targu-Mures",
            "type": "Responsive Employee",
            "logo": "https://cdn.bestjobs.eu/static/build/images/bj-geko-green-blue.a5ca84dd.png",
            'salary': "1750 - 1850€",
            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        },
        {
            "id": 1,
            "name": "Python Developer",
            "firma": "Bestjobs",
            'salary': "1750 - 1850€",
            "location": "Hartau",
            "type": "Responsive Employee",
            "logo": "https://cdn.bestjobs.eu/static/build/images/bj-geko-green-blue.a5ca84dd.png",
            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "rating": 50,
        },
        {
            "id": 2,
            "name": "PHP Developer nd lorem ipsum more rows",
            "firma": "Bestjobs",
            "rating": 29,
            'salary': "1750 - 1850€",
            "location": "Cluj Napoca",
            "type": "Responsive Employee",
            "logo": "https://cdn.bestjobs.eu/static/build/images/bj-geko-green-blue.a5ca84dd.png",
            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",

        },
        {
            "id": 3,
            'salary': "1750 - 1850€",
            "name": "Full-Stack Web Developer",
            "firma": "Bestjobs",
            "rating": 11,
            "location": "Cluj Napoca",
            "type": "Responsive Employee",
            "logo": "https://cdn.bestjobs.eu/static/build/images/bj-geko-green-blue.a5ca84dd.png",
            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",


        },
        {
            "id": 4,
            "name": "Java Junior Position nd lorem ipsum more rows" ,
            "firma": "Bestjobs",
            "rating": 99,

            'salary': "1750 - 1850€",
            "location": "Cluj Napoca",
            "type": "Responsive Employee",
            "logo": "https://cdn.bestjobs.eu/static/build/images/bj-geko-green-blue.a5ca84dd.png",
            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",


        },
        {
            "id": 5,
            "name": "PHP Fullstack-Developer",
            "firma": "Bestjobs",
            "location": "Cluj Napoca",
            "type": "Responsive Employee",
            "logo": "https://cdn.bestjobs.eu/static/build/images/bj-geko-green-blue.a5ca84dd.png",
            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'salary': "1750 - 1850€",
            "rating": 59,


        },
        {
            "id": 6,
            "name": "Full-Stack Web Developer nd lorem ipsum more rows",
            "firma": "Bestjobs",
            "rating": 33,
            "location": "Timisoara",
            "type": "Responsive Employee",
            "logo": "https://cdn.bestjobs.eu/static/build/images/bj-geko-green-blue.a5ca84dd.png",
            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'salary': "1750 - 1850€",


        },
        {
            "id": 7,
            "name": "Full-Stack Web Developer",
            "firma": "Bestjobs",
            "rating": 77,
            "location": "Arad",
            "type": "Responsive Employee",
            "logo": "https://cdn.bestjobs.eu/static/build/images/bj-geko-green-blue.a5ca84dd.png",
            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'salary': "1750 - 1850€",


        }
       ]
    const [whatInput, setWhatInput] = useState('');
    const [whereInput, setWhereInput] = useState('');
    const [jobsActive, setActiveJobs] = useState(jobsDefault);


    const onSearch = () => {

        if (!whatInput && !whereInput) {
            setActiveJobs(jobsDefault);
            return;
        };
        let jobs;
        if (whatInput && whereInput) {
            jobs = jobsDefault.filter(job => job.name.toLowerCase().includes(whatInput.toLowerCase()) && job.location.toLowerCase().includes(whereInput.toLowerCase()))
        } else {
            jobs = whatInput ? jobsDefault.filter(job => job.name.toLowerCase().includes(whatInput.toLowerCase())) : jobsDefault.filter(job => job.location.toLowerCase().includes(whereInput.toLowerCase()));
        }
        console.log(jobs);
        setActiveJobs(jobs);
    }

    useEffect(() => {
        if (!whatInput && !whereInput) {
            setActiveJobs(jobsDefault);
        }
    }, [whereInput, whatInput])

    return (
        <>
            <section className={"searchBar"} >
                <div className={"container"}>
                    <div className={"searchBar__wrapper"}>
                        <div className={"row"}>


                        <div className={"col-lg-5 col-md-4 searchBar__wrapper--item "}>
                             <p>What are you searching for?</p>
                            <Form.Group>
                                <FontAwesomeIcon icon={faSearch} />
                                <Form.Control onChange={event => setWhatInput(event.target.value)} type="text" placeholder="Keyword" />
                            </Form.Group>
                        </div>
                        <div className={"col-lg-5 col-md-5 searchBar__wrapper--item "}>
                            <p>Where are you searching?</p>
                            <Form.Group>
                                <FontAwesomeIcon icon={faLocationArrow} />
                                <Form.Control onChange={event => setWhereInput(event.target.value)} type="text" placeholder="City/County/Country"/>
                            </Form.Group>
                        </div>
                        <div className={"col-lg-2 col-md-3 align-bottom searchBar__wrapper--item "}>
                            <button onClick={onSearch} className={"btn-type-one btn btn-primary"}> <FontAwesomeIcon icon={faSearch} /> Search</button>
                        </div>
                    </div>
                    </div>
                </div>
            </section>
            <JobsListing jobs={jobsActive} />
        </>
    );
}

export default SearchBar;
