import React from 'react';
import '../../scss/layout/Header.scss';
import '../../scss/layout/Footer.scss';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faFacebook, faLinkedin, faLaravel, faApple, faGooglePlay , } from "@fortawesome/free-brands-svg-icons"
import { faGlobe } from '@fortawesome/free-solid-svg-icons'

import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'
const Footer = () => {
    var d = new Date();
    var year = d.getFullYear();
    return (
        <>
            <section className={"mastfooter"}>
                <div className={"container"}>
                    <div className={"mastfooter__wrapper"}>
                        <div className={"mastfooter__wrapper--item"}>
                            <a className={'logo'} href="{}">
                                <img alt={'BestJobs'}
                                     src="https://cdn.bestjobs.eu/static/build/images/bj-geko-green-blue.a5ca84dd.png"/>
                            </a>
                        </div>
                        <div className={"mastfooter__wrapper--item"}>
                            <div className={"mastfooter__wrapper--item--group"}>
                                <h4>Candidates</h4>
                                <ul>
                                    <li><a href="{}">Site map</a></li>
                                    <li><a href={"tel:0317104330"}>Support (+40) 31 710 4330</a></li>
                                    <li><a href="{}">Conditions</a></li>
                                    <li><a href="{}">Confidentiality</a></li>
                                </ul>
                            </div>
                            <div className={"mastfooter__wrapper--item--group"}>

                                <h4>Candidates</h4>
                                <ul>
                                    <li><a href="{}">Add job</a></li>
                                    <li><a href="{}">Pricing</a></li>
                                    <li><a href="{}">I want an employer account</a></li>
                                </ul>
                            </div>

                        </div>
                        <div className={"mastfooter__wrapper--item"}>
                            <div className={"mastfooter__wrapper--item--group"}>
                                <h4>Follow us on social media: </h4>
                                <div className={"social"}>

                                    <a href="{}" className={"tooltip-custom"}>
                                        <span className={"icon"}>
                                            <FontAwesomeIcon icon={faFacebook}/>
                                        </span>
                                        <p>Follow us on facebook</p>
                                    </a>

                                    <a href="{}" className={"tooltip-custom"}>
                                        <span className={"icon"}>
                                            <FontAwesomeIcon icon={faLinkedin}/>
                                        </span>
                                        <p>Follow us on Linkedin</p>
                                    </a>

                                    <a href="{}" className={"tooltip-custom"}>
                                        <span className={"icon"}>
                                            <FontAwesomeIcon icon={faLaravel}/>
                                        </span>
                                        <p>Follow us on Learn</p>
                                    </a>

                                </div>

                                <h4>Download the Bestjobs app</h4>
                                <div className={"download"}>
                                    <a href="{}">
                                        <span className={"icon"}>
                                            <FontAwesomeIcon icon={faApple}/>
                                        </span>
                                        <div className={"text"}>
                                            <p>Available on the</p>
                                            <p>App Store</p>
                                        </div>

                                    </a>

                                    <a href="{}">
                                        <span className={"icon"}>
                                            <FontAwesomeIcon icon={faGooglePlay}/>
                                        </span>
                                        <div className={"text"}>

                                        <p>Get it on</p>
                                        <p>Google Play</p>
                                        </div>
                                    </a>

                                </div>
                            </div>


                        </div>
                        <div className={"mastfooter__wrapper--item"}>
                            <div className={"mastfooter__wrapper--item--group"}>
                            <div className={"language"}>
                            <FontAwesomeIcon icon={faGlobe}/>
                            <DropdownButton id="dropdown-item-button" title="English">
                                <Dropdown.ItemText>French</Dropdown.ItemText>
                                <Dropdown.Item as="button">Italian</Dropdown.Item>
                                <Dropdown.Item as="button">Magyar</Dropdown.Item>
                                <Dropdown.Item as="button">Deutsch</Dropdown.Item>
                            </DropdownButton>
                            </div>
                        </div>
                        </div>

                    </div>

                </div>
            </section>
            <section className={"mastfooter__copyright"}>
                <p>© {year} BESTJOBS RECRUTARE SA</p>
            </section>
        </>
    );
}

export default Footer;
