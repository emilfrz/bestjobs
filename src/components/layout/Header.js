import React, {useState} from 'react';
import '../../scss/layout/Header.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSignInAlt } from '@fortawesome/free-solid-svg-icons'
import { faLock } from '@fortawesome/free-solid-svg-icons'

const Header = (props) => {
    const [openHeaderVariable, setOpenHeader] = useState(false);
    const toggleClass = () => {
        let currentState  = openHeaderVariable
        setOpenHeader(!currentState);
    };

        return (
        <>
            <section className={ props.scrollVal > 300 ? 'sticky mastheader' : 'mastheader' } >
                <div className={'container'}>
                <div className={'mastheader__wrapper'}>
                    <div className={'mastheader__wrapper--left'}>
                        <a className={'logo'} href="{}">

                            <img alt={'BestJobs'}
                                 src="https://cdn.bestjobs.eu/static/build/images/bj-geko-green-blue.a5ca84dd.png"/>
                        </a>
                    </div>
                    <div className={'mastheader__wrapper--right'}>

                        <div onClick={() => toggleClass()} className={"hamburger-icon-container "+ (openHeaderVariable ? "hamburger-active": "")}>
                            <span className={"hamburger-icon"}></span>
                        </div>


                        <ul className={'actions '+ (openHeaderVariable ? "show": "")}>
                            <li><a href="{}"><strong>Recruiting?</strong> Add job</a> </li>
                            <li><a href="{}"> <FontAwesomeIcon icon={faSignInAlt} />Sign in</a> </li>
                            <li><a href="{}"> <FontAwesomeIcon icon={faLock} />Sign up</a> </li>
                        </ul>
                    </div>
                </div>
                </div>
            </section>


        </>
    );
}



export default Header;
