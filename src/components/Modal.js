import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import '../scss/components/ModalJob.scss';



const CustomModal = (props) => {
    return (
        <>
            <Modal className={"job__modal"} show={props.open} onHide={props.onClose}>
                <Modal.Header closeButton>
                    <Modal.Title><div className={"company__logo"}><img alt={props.title} src={props.logo}/></div><p>{props.title}</p> </Modal.Title>
                </Modal.Header>
                <Modal.Body>{props.children}   </Modal.Body>
                <Modal.Footer>

                    <Button  className="btn-type-one" variant="primary" onClick={props.onClose}>
                        Apply Now
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default CustomModal;
