import React from 'react';
import '../scss/layout/Header.scss';
import '../scss/components/ContentTop.scss';

const ContentTop = () => {
    return (
        <>
            <section className={"contentTop"}>
                <div className={"container"}>
                    <div className={"contentTop__wrapper"}>
                            <p>Discover your best!</p>
                            <p>More jobs, more chances to get hired. Find the job that makes you happy!</p>

                    </div>
                </div>
            </section>
        </>
    );
}

export default ContentTop;
