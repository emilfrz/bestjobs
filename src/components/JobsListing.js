import React, {useEffect, useState} from 'react';
import Modal from './Modal';
import '../scss/abstracts/_core.scss'
import '../scss/layout/Header.scss';
import '../scss/components/JobsListing.scss';
import '../scss/components/OverwriteBootstrap.scss';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faHeart} from '@fortawesome/free-solid-svg-icons'

const JobsListing = ({jobs}) => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [activeItem, setActiveItem] = useState("");
    const [loadedAllJobs, setLoadedAllJobs] = useState(false);
    const [jobsActive, setActiveJobs] = useState(jobs.slice(0, 4));

    useEffect(() => {
        setActiveJobs(jobs.slice(0, 4))
        setLoadedAllJobs(jobs.length <= 4)
    }, [jobs])

    const closeModal = () => {
        setIsModalVisible(false);
    }
    const openModal = (i) => {
        setIsModalVisible(true);
        setActiveItem(i);
    }
    const loadMoreJobs = () => {
        setActiveJobs(jobs)
        setLoadedAllJobs(true);
    }

    return (
        <>
            <Modal open={isModalVisible} id={activeItem.id} logo={activeItem?.logo} title={activeItem?.name}
                   onClose={closeModal}>
                {activeItem.location && (<div className={"location"}>
                    Location:{activeItem.location}
                </div>)}
                {activeItem.description && (<div className={"content"}>
                    {activeItem.description}
                </div>)}
            </Modal>

            <section className={'jobs'}>
                <div className={'container'}>
                    <h3 className={'jobs__topTitle'}>Jobs in your location.</h3>
                    <div className={'jobs__wrapper'}>
                        {jobsActive.map((i, index) =>
                            <div key={index} className={'jobs__wrapper--card'}>
                                <a className={"card__favorite"} href="{}">
                                    <FontAwesomeIcon icon={faHeart}/>
                                </a>
                                <div className={"card__top"}>
                                    <img alt={i.firma} className={"logo"} src={i.logo}/>
                                </div>
                                <div className={"card__content"}>
                                    <h3 className={"card__content--company"}>{i.firma}</h3>

                                    <div className={"star-ratings-sprite"}>
                                    <span style={{width: `${i?.rating}%`}}
                                          className={"star-ratings-sprite-rating"}>
                                    </span>
                                    </div>
                                    <h3 className={"card__content--title"}>{i.name}</h3>
                                </div>
                                <div className={"card__action"}>
                                    <div className={"card__action__wrapper"}>
                                        {i.location && (<div className={"card__action__wrapper--left"}>
                                            <p> Location: </p>
                                            <span className={"location"}>{i.location}</span>
                                        </div>)}
                                        {i.salary && (<div className={"card__action__wrapper--right"}>
                                            <p> Salary: </p>
                                            <span className={"location"}>{i.salary}</span>
                                        </div>)}
                                    </div>
                                    <button className={"btn-type-one--inversed btn btn-primary"}
                                            onClick={() => openModal(i)}>Details
                                    </button>
                                </div>
                            </div>
                        )}
                    </div>

                    {!loadedAllJobs && ( <div className={"jobs__action"}>
                        <button className={"btn-type-one btn btn-primary"} onClick={() => loadMoreJobs()}>See all
                            jobs
                        </button>
                    </div>)}

                    {!jobs.length > 0 && (<div className={"jobs__noJobs"}>
                        <p>No jobs founded by your criteria.</p>
                    </div>)}


                </div>
            </section>


        </>
    );
}


export default JobsListing;
