import React, {useState} from 'react';
import Header from '../components/layout/Header';
import SearchBar from "../components/SearchBar";
import ContentTop from "../components/ContentTop";
import Footer from "../components/layout/Footer";

const Home = () => {
    const [scrollValue, setScrollValue] = useState(0);
    window.onscroll =()=> {
        setScrollValue(window.scrollY);
    }

  return (
      <>
          <Header scrollVal={scrollValue}/>
          <ContentTop/>
          <SearchBar/>
          <Footer/>
      </>
  );
}

export default Home;
